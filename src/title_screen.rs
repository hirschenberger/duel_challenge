use crate::duel_challenge::{DuelResult, InputState, Stage, World, CONTINUE_KEY};
use ggez::mint::Vector2;
use ggez::{
    audio::{self, SoundSource},
    graphics, Context, GameResult,
};
use std::time::Duration;

pub struct TitleScreen {
    pub title_background: graphics::Image,
    pub drums: audio::Source,
    pub play_drums: bool,
    pub input_state: InputState,
}

impl TitleScreen {
    pub fn new(ctx: &mut Context) -> GameResult<Self> {
        Ok(TitleScreen {
            title_background: graphics::Image::new(ctx, "/title_background.png")?,
            drums: audio::Source::new(ctx, "/marching_drums.ogg")?,
            play_drums: false,
            input_state: InputState::default(),
        })
    }
}

impl Stage<World> for TitleScreen {
    fn draw(&mut self, world: &mut World, ctx: &mut Context) -> GameResult {
        let frame = world.game_frame;
        let bg = &self.title_background;
        graphics::draw(
            ctx,
            bg,
            graphics::DrawParam::default()
                .dest(world.game_frame.point())
                .scale(Vector2::from([
                    frame.w / bg.width() as f32,
                    -frame.h / bg.height() as f32,
                ])),
        )?;

        let (w, h) = graphics::drawable_size(ctx);
        let text = graphics::Text::new(("Press Space to start game", world.main_font, 32.0));
        let text_width = text.width(ctx) as f32;
        graphics::draw(
            ctx,
            &text,
            (
                [w * 0.5 - text_width * 0.5, h - 150.],
                0.0,
                [1.0f32, 0.0, 0.0, 1.0].into(),
            ),
        )?;

        Ok(())
    }

    fn update(&mut self, _world: &mut World, _ctx: &mut Context) -> GameResult<DuelResult> {
        if !self.play_drums {
            self.drums.set_fade_in(Duration::from_millis(1000));
            self.drums.set_repeat(true);
            self.drums.play()?;
            self.play_drums = true;
        }

        if self.input_state.key == Some(CONTINUE_KEY) {
            return Ok(DuelResult::Ignore);
        }
        Ok(DuelResult::None)
    }

    fn input_state(&mut self, _world: &mut World, input_state: InputState) {
        self.input_state = input_state;
    }
}

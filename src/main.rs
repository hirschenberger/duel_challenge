use ggez::event::{self};
use ggez::{conf, ContextBuilder, GameResult};

mod duel_challenge;
mod reaction_game;
mod title_screen;

use crate::duel_challenge::{DuelChallenge, World};

fn main() -> GameResult {
    let (mut ctx, mut event_loop) = ContextBuilder::new("Duel Challenge", "Falco Hirschenberger")
        .add_resource_path("./resources")
        .window_setup(conf::WindowSetup::default().title("Duel Challenge!"))
        .window_mode(conf::WindowMode::default().dimensions(1024.0, 576.0))
        .build()?;
    let mut game: DuelChallenge<World> = DuelChallenge::new(&mut ctx)?;

    event::run(&mut ctx, &mut event_loop, &mut game)?;
    Ok(())
}

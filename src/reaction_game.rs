use ggez::{graphics, Context, GameResult};

use rand::Rng;
use std::rc::Rc;
use std::time::{Duration, Instant};

use crate::duel_challenge::{DuelResult, InputState, Stage, World, PLAYER1_KEY, PLAYER2_KEY};

pub struct ReactionGame {
    input_state: InputState,
    now: Option<Instant>,
    timeout: Duration,
    elapsed: bool,
}

impl ReactionGame {
    pub fn new(_ctx: &mut Context) -> GameResult<Self> {
        let mut rng = rand::thread_rng();
        Ok(ReactionGame {
            input_state: InputState::default(),
            now: None,
            timeout: Duration::from_millis(rng.gen_range(3000, 10000)),
            elapsed: false,
        })
    }

    pub fn start_timer(&mut self) {
        self.now = Some(Instant::now());
    }
}

impl Stage<World> for ReactionGame {
    fn draw(&mut self, world: &mut World, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, graphics::BLACK);
        let (w, _h) = graphics::drawable_size(ctx);
        let frame = world.game_frame;
        let text = graphics::Text::new(("Shoot when background white!", world.main_font, 32.0));
        let text_width = text.width(ctx) as f32;
        graphics::draw(
            ctx,
            &text,
            (
                [w * 0.5 - text_width * 0.5, frame.top() + 50.],
                0.0,
                graphics::WHITE,
            ),
        )?;

        if self.elapsed {
            graphics::clear(ctx, graphics::WHITE);
        }
        Ok(())
    }

    fn update(&mut self, world: &mut World, _ctx: &mut Context) -> GameResult<DuelResult> {
        // game hs not started yet
        if self.now.is_none() {
            self.start_timer();
        }

        let elapsed = self.now.unwrap().elapsed() > self.timeout;
        if let Some(k) = self.input_state.key {
            let result = match k {
                PLAYER1_KEY if elapsed => DuelResult::Winner(Rc::clone(&world.player1)),
                PLAYER1_KEY if !elapsed => DuelResult::Loser(Rc::clone(&world.player1)),
                PLAYER2_KEY if elapsed => DuelResult::Winner(Rc::clone(&world.player2)),
                PLAYER2_KEY if !elapsed => DuelResult::Loser(Rc::clone(&world.player2)),
                _ => DuelResult::None,
            };
            return Ok(result);
        };

        self.elapsed = elapsed;
        Ok(DuelResult::None)
    }

    fn input_state(&mut self, _world: &mut World, input_state: InputState) {
        self.input_state = input_state;
    }
}

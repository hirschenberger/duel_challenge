use ggez::{
    audio::{self, SoundSource},
    event::{self, KeyCode, KeyMods},
    graphics, timer, Context, GameResult,
};
//use nalgebra as na;
use std::cell::Cell;
use std::cmp::PartialEq;
use std::rc::Rc;

use crate::reaction_game::ReactionGame;
use crate::title_screen::TitleScreen;

pub const PLAYER1_KEY: KeyCode = KeyCode::A;
pub const PLAYER2_KEY: KeyCode = KeyCode::L;
pub const CONTINUE_KEY: KeyCode = KeyCode::Space;

#[derive(PartialEq)]
pub enum DuelResult {
    None,
    Ignore,
    Winner(Rc<Player>),
    Loser(Rc<Player>),
}

pub trait Stage<W> {
    fn draw(&mut self, world: &mut W, ctx: &mut Context) -> GameResult;
    fn update(&mut self, world: &mut W, ctx: &mut Context) -> GameResult<DuelResult>;
    fn input_state(&mut self, world: &mut W, input_state: InputState);
}

#[derive(Debug, Default)]
pub struct InputState {
    pub key: Option<KeyCode>,
}

#[derive(PartialEq)]
pub struct Player {
    pub name: String,
    pub score: Cell<isize>,
    pub image: graphics::Image,
}

impl Player {
    pub fn score_up(&self) {
        self.score.set(self.score.get() + 1);
    }

    pub fn score_down(&self) {
        self.score.set(self.score.get() - 1);
    }
}

pub struct World {
    pub player1: Rc<Player>,
    pub player2: Rc<Player>,
    pub main_font: graphics::Font,
    pub game_frame: graphics::Rect,
}

pub struct DuelChallenge<W> {
    frame: graphics::Image,
    title: graphics::Image,
    shot: audio::Source,
    ricochet: audio::Source,
    world: W,
    stages: Vec<Box<dyn Stage<W>>>,
    input_state: InputState,
    result: DuelResult,
}

impl DuelChallenge<World> {
    pub fn new(ctx: &mut Context) -> GameResult<Self> {
        Ok(DuelChallenge {
            frame: graphics::Image::new(ctx, "/frame.png")?,
            title: graphics::Image::new(ctx, "/title.png")?,
            shot: audio::Source::new(ctx, "/shot.mp3")?,
            ricochet: audio::Source::new(ctx, "/shot_ricochet.mp3")?,
            world: World {
                game_frame: [158.0, 114.0, 866.0 - 158.0, 114.0 - 466.0].into(),
                player1: Rc::new(Player {
                    name: "Player 1".to_owned(),
                    score: Cell::new(0),
                    image: graphics::Image::new(ctx, "/player1.png")?,
                }),
                player2: Rc::new(Player {
                    name: "Player 2".to_owned(),
                    score: Cell::new(0),
                    image: graphics::Image::new(ctx, "/player1.png")?,
                }),
                main_font: graphics::Font::new(ctx, "/RobotoSlab_Bold.ttf")?,
            },
            stages: vec![
                Box::new(ReactionGame::new(ctx)?),
                Box::new(TitleScreen::new(ctx)?),
            ],
            input_state: InputState::default(),
            result: DuelResult::None,
        })
    }

    pub fn draw_winner(&self, ctx: &mut Context, player: &Player) -> GameResult {
        graphics::clear(ctx, [0.0, 1.0, 0.0, 1.0].into());
        let (w, h) = graphics::drawable_size(ctx);

        graphics::draw(
            ctx,
            &player.image,
            graphics::DrawParam::default()
                .dest([w * 0.5 - player.image.width() as f32 * 0.25, h * 0.25])
                .scale([0.5, 0.5]),
        )?;
        let text =
            graphics::Text::new((format!("{} wins!", player.name), self.world.main_font, 50.0));
        let text_width = text.width(ctx) as f32;
        graphics::draw(
            ctx,
            &text,
            ([w * 0.5 - text_width * 0.5, h * 0.5], 0.0, graphics::BLACK),
        )?;
        Ok(())
    }

    pub fn draw_loser(&self, ctx: &mut Context, player: &Player) -> GameResult {
        graphics::clear(ctx, [1.0, 0.0, 0.0, 1.0].into());
        let (w, h) = graphics::drawable_size(ctx);
        graphics::draw(
            ctx,
            &player.image,
            graphics::DrawParam::default()
                .dest([w * 0.5 - player.image.width() as f32 * 0.25, h * 0.25])
                .scale([0.5, 0.5]),
        )?;

        let text =
            graphics::Text::new((format!("{} lost!", player.name), self.world.main_font, 50.0));
        let text_width = text.width(ctx) as f32;
        graphics::draw(
            ctx,
            &text,
            ([w * 0.5 - text_width * 0.5, h * 0.5], 0.0, graphics::BLACK),
        )?;
        Ok(())
    }

    pub fn draw_final_score(&self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, [0.0, 0.0, 1.0, 1.0].into());
        Ok(())
    }
}

impl event::EventHandler for DuelChallenge<World> {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        const DESIRED_FPS: u32 = 60;
        while timer::check_update_time(ctx, DESIRED_FPS) {
            if let Some(stage) = self.stages.last_mut() {
                stage.update(&mut self.world, ctx).and_then(|result| {
                    self.result = result;
                    match &self.result {
                        DuelResult::None => Ok(()),
                        DuelResult::Loser(p) => {
                            self.ricochet.play()?;
                            p.score_down();
                            self.stages.pop();
                            Ok(())
                        }
                        DuelResult::Winner(p) => {
                            self.shot.play()?;
                            p.score_up();
                            self.stages.pop();
                            Ok(())
                        }
                        _ => {
                            self.stages.pop();
                            Ok(())
                        }
                    }
                })?
            }
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, graphics::BLACK);
        let (w, h) = graphics::drawable_size(ctx);
        match &self.result {
            DuelResult::Loser(p) => self.draw_loser(ctx, &*p)?,
            DuelResult::Winner(p) => self.draw_winner(ctx, &*p)?,
            _ => match self.stages.last_mut() {
                None => self.draw_final_score(ctx)?,
                Some(stage) => {
                    stage.draw(&mut self.world, ctx)?;
                }
            },
        }
        graphics::draw(ctx, &self.frame, graphics::DrawParam::default())?;

        graphics::draw(
            ctx,
            &self.title,
            graphics::DrawParam::default().dest([w * 0.5 - self.title.width() as f32 * 0.5, 5.]),
        )?;

        let text = graphics::Text::new(("Player 1 key: A", self.world.main_font, 32.0));
        let text_width = text.width(ctx) as f32;
        graphics::draw(
            ctx,
            &text,
            (
                [w * 0.20 - text_width * 0.5, h - 105.],
                0.0,
                graphics::BLACK,
            ),
        )?;

        let text = graphics::Text::new(("Player 2 key: L", self.world.main_font, 32.0));
        let text_width = text.width(ctx) as f32;
        graphics::draw(
            ctx,
            &text,
            (
                [w * 0.80 - text_width * 0.5, h - 105.],
                0.0,
                graphics::BLACK,
            ),
        )?;

        let world = &self.world;
        // TODO: hiding the score on the main screen does not work as expected
        //if world.player1.score.get() != 0 && world.player2.score.get() != 0 {
        let text =
            graphics::Text::new((world.player1.score.get().to_string(), world.main_font, 70.0));
        let text_height = text.height(ctx) as f32;
        graphics::draw(
            ctx,
            &text,
            (
                [w * 0.01, h * 0.5 - text_height * 0.5],
                0.0,
                graphics::BLACK,
            ),
        )?;
        let text =
            graphics::Text::new((world.player2.score.get().to_string(), world.main_font, 70.0));
        let text_height = text.height(ctx) as f32;
        let text_width = text.width(ctx) as f32;
        graphics::draw(
            ctx,
            &text,
            (
                [w * 0.99 - text_width, h * 0.5 - text_height * 0.5],
                0.0,
                graphics::BLACK,
            ),
        )?;
        //}

        graphics::present(ctx)?;
        timer::yield_now();
        Ok(())
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: KeyCode,
        _keymod: KeyMods,
        _repeat: bool,
    ) {
        match keycode {
            KeyCode::Escape => event::quit(ctx),
            code => match self.stages.last_mut() {
                Some(stage) => stage.input_state(&mut self.world, InputState { key: Some(code) }),
                None => (),
            },
        }
    }

    fn key_up_event(&mut self, _ctx: &mut Context, _keycode: KeyCode, _keymod: KeyMods) {
        self.input_state.key = None;
    }
}
